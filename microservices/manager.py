from microservices.models import Tag


def gettags():
    tags_query = Tag.objects.all()
    tags = []
    for tag in tags_query:
        tags.append(tag.name)
    return tags


