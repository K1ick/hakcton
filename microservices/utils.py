from .models import Service


def searchfind(string):
    services = Service.objects.all()
    matches = []
    for service in services:
        if service.name.startswith(string.lower()):
            matches.append(service.name)
    return matches
