from django.http import JsonResponse, HttpResponse
from django.shortcuts import render, redirect
from microservices.models import Service, Tag
from microservices.manager import gettags
from microservices import utils

def sendtags(request):
    return JsonResponse({"tags": gettags()})


def sendDet(request):
    if "name" in request.GET:
        service = Service.objects.get(name=request.GET["name"])
        return JsonResponse(service.sendDetails())
    else:
        return redirect(request.get_full_path())


def index(request, **kwargs):
    return render(request, 'index.html')


def tagcontent(request):
    if "tag" in request.GET:
        tag = Tag.objects.get(name=request.GET['tag'])
        services = []
        for service in tag.service_set.all():
            services.append(service.name)
        return JsonResponse({"services": services})
    else:
        return redirect(request.get_full_path())


def findsearch(request):
    return JsonResponse({"matches": utils.searchfind(request.GET['queStr'])})


