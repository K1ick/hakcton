from django.contrib import admin
from .models import Service, Tag, Params

# Register your models here.
admin.site.register(Service)
admin.site.register(Tag)
admin.site.register(Params)
