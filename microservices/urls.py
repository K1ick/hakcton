from django.conf.urls.static import static
from django.urls import path, re_path
from microservices import views
from myproject import settings

urlpatterns = [
    path('getSuggest/', views.findsearch),
    path('getTags/', views.sendtags),
    path('getDetails/', views.sendDet),
    path('getMicros/', views.tagcontent),
    path('<str:name>/', views.index),
    path('', views.index),


]
