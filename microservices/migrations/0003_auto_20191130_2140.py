# Generated by Django 2.2.6 on 2019-11-30 21:40

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('microservices', '0002_auto_20191130_2132'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='service',
            name='api',
        ),
        migrations.AddField(
            model_name='params',
            name='service',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, to='microservices.Service'),
            preserve_default=False,
        ),
    ]
