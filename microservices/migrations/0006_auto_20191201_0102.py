# Generated by Django 2.2.6 on 2019-12-01 01:02

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('microservices', '0005_auto_20191130_2304'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='service',
            name='Connections',
        ),
        migrations.AddField(
            model_name='service',
            name='Connections',
            field=models.ManyToManyField(related_name='_service_Connections_+', to='microservices.Service'),
        ),
    ]
