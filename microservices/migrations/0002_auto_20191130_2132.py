# Generated by Django 2.2.6 on 2019-11-30 21:32

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('microservices', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='service',
            old_name='API',
            new_name='api',
        ),
    ]
