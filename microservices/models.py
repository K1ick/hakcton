import json

from django.db import models
from django.contrib.postgres.fields import ArrayField, JSONField
from comands.models import Comand


class Tag(models.Model):
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name


class Service(models.Model):
    choices = [
        ("dev", "dev"),
        ("deprecated", "deprecated"),
        ("normal", "normal")
    ]
    name = models.CharField(max_length=255)
    description = models.TextField(blank=True)
    choices = models.CharField(max_length=20, choices=choices)
    Connections = models.ManyToManyField("self")
    comand = models.ManyToManyField(Comand)
    url = models.CharField(max_length=255)
    tags = models.ManyToManyField(Tag)

    def sendDetails(self):
        tags = []
        params = []
        parametors = self.params_set.all()
        connects = self.Connections.all()
        related = []
        respTeam = []
        for t in self.comand.all():
            respTeam.append(t.name)
        for connect in connects:
            related.append({"name": connect.name,
                            "description": connect.description})
        for p in parametors:
            params.append({
                "name": p.name,
                "description": p.description,
                "type": p.type,
                "params": p.params
            })

        for tag in self.tags.all():
            tags.append(tag.name)
        return {
            "details": {
                "name": self.name,
                "description": self.description,
                "respTeam": respTeam[0],
                "methods": params,
                "tags": tags,
                "url": self.url,
                "current": self.choices,
                "related": related
            }
        }

    def getMicros(self):
        parametors = self.params_set.all()
        p = []
        for param in parametors:
            p.append(param.getmicros())
        return {"metods": p
                }

    def __str__(self):
        return self.name


class Params(models.Model):
    choice = [
        ("GET", "GET"),
        ("POST", "POST"),
        ("PUT", "PUT"),
        ("DELETE", "DELETE")
    ]
    name = models.CharField(max_length=255)
    description = models.TextField()
    type = models.CharField(max_length=10, choices=choice)
    service = models.ForeignKey(Service, on_delete=models.CASCADE)
    params = JSONField(blank=True, null=True)
    response_description = models.TextField(blank=True)

    def getmicros(self):
        return {
            "name": self.name,
            "description": self.description,
            "params": self.params,
            "response_description": self.response_description
        }

    def __str__(self):
        return self.name
