import json

from django.http import JsonResponse
from django.shortcuts import render, redirect
from django.views.decorators.csrf import csrf_protect

from .models import Comand
from microservices.models import Service


# Create your views here.
@csrf_protect
def addcommand(request):
    unicode = request.body.decode('utf-8')
    data = json.loads(unicode)
    comand = Comand(name=data["name"], agent=data['agent'])
    comand.save()
    return redirect("/")


def getteam(request):
    services = Service.objects.get(name=request.GET['name'])
    comands = services.comand.all()
    comand = comands[0]
    return JsonResponse(comand.getteam())
