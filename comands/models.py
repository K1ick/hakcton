import json

from django.db import models
from django.contrib.postgres.fields import JSONField


class Comand(models.Model):
    name = models.CharField(max_length=255)
    agent = JSONField(blank=True, null=True)

    def getteam(self):
        return {
            "teamName": self.name,
            "agent": self.agent
        }

    def __str__(self):
        return self.name
