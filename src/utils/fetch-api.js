const baseGet = {
  method: "GET",
  headers: {
    'Content-Type': 'application/json',
  },
  credentials: 'same-origin',
}

const csrfToken = document.cookie.replace(/(?:(?:^|.*;\s*)csrftoken\s*\=\s*([^;]*).*$)|^.*$/, "$1");

const basePost = {
  method: "POST",
  headers: {
    'Content-Type': 'application/json',
    'X-CSRFToken': csrfToken,
  },
  credentials: 'same-origin',
}


export const getDetails = async (name) => {
    let reqInit = {...baseGet};
    let response = await fetch(`/getDetails/?name=${name}`, reqInit);
    return response.json();
}

export const getTags = async () => {
    let reqInit = {...baseGet};
    let response = await fetch(`/getTags`, reqInit);
    return response.json();
}

export const getMicros = async (tag) => {
    let reqInit = {...baseGet};
    let response = await fetch(`/getMicros/?tag=${tag}`, reqInit);
    return response.json();
}

export const searchQuery = async (queryString) => {
    let reqInit = {...baseGet};
    let response = await fetch(`/getSuggest/?queStr=${queryString}`, reqInit);
    return response.json();
}

export const getCommand = async (name) => {
    let reqInit = {...baseGet};
    let response = await fetch(`/command/getCommand/?name=${name}`, reqInit);
    return response.json();
}

export const addCommand = async (obj) => {
  let reqInit = {...basePost};
  reqInit.body = JSON.stringify(obj);
  let response = await fetch('/command/setCommand/', reqInit);
}

export const addMicro = async (obj) => {
  let reqInit = {...basePost};
  reqInit.body = JSON.stringify(obj);
  let response = await fetch('/setMicro/', reqInit);
}
