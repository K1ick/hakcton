import React, { Component } from 'react';
import "./MethodDescription.css";

export default class MethodDescription extends Component {
    constructor(props) {
        super(props);
        this.handleClick = this.handleClick.bind(this)
    }
    state = {
        show: false
    }
    handleClick() {
        this.setState({show: !this.state.show})
    }
    render() {
        return (
            <div className="dropdown-wrapper">
                <div className="method-type">{this.props.method.type}</div>
                <div className="method-name" onClick={this.handleClick}>{this.props.method.name}</div>
                    {this.state.show ? <div><div className="method-description">{this.props.method.description}</div><div className="method-form">{this.props.method.params.params.map(param=>{
                        return <label className="param-label">{param.name}<input type={param.type}></input></label>
                    })}</div></div> : <></>}
                <div className="method-params">
                    {this.props.method.params.params.map(param=>{
                        return <div className="param">Name: {param.name}.Type: {param.type}. Is required: {param.required}</div>
                    })}
                </div>
            </div>
        )
    }
}
