import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import "./LinkItem.css";

export default class LinkItem extends Component {
    render() {
        const name = this.props.name;

        return (
        <li className="link-item">
            <Link to={'/'+name}>{name}</Link>
        </li>
        );
    }
}
