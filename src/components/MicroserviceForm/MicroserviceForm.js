import React, { Component } from "react";
import Button from "components/Button";
import "./MicroserviceForm.css";
import { addMicro } from "../../utils/fetch-api";

export default class MicroserviceForm extends Component {
  state = {
    name: "",
    description: "",
    respTeam: "",
    tags: [],
    methods: [],
    related: [],
    current: "",
    url: ""
  };

  onChangeInput = e => {
    this.setState({ [e.target.name]: e.target.value });
  };

  save = () => {
    addMicro(this.state);
  };

  render() {
    return (
      <form className="form">
        <label for="name">Name:</label>
        <input
          className="form-input"
          id="name"
          name="name"
          onChange={this.onChangeInput}
        />
        <label for="description">Description:</label>
        <textarea
          className="form-textarea"
          id="description"
          name="description"
          onChange={this.onChangeInput}
        />
        <label for="team">RespTeam:</label>
        <input
          className="form-input"
          id="team"
          name="respTeam"
          onChange={this.onChangeInput}
        />
        <label for="tags">Tags</label>
        <input
          className="form-input"
          id="tags"
          name="tags"
          onChange={this.onChangeInput}
        />
        <label for="methods">Methods:</label>
        <input
          className="form-input"
          id="methods"
          name="methods"
          onChange={this.onChangeInput}
        />
        <label for="related">Related:</label>
        <input
          className="form-input"
          id="related"
          name="related"
          onChange={this.onChangeInput}
        />
        <label for="current">Current:</label>
        <input
          className="form-input"
          id="current"
          name="current"
          onChange={this.onChangeInput}
        />
        <label for="url">Url:</label>
        <input
          className="form-input"
          id="url"
          name="url"
          onChange={this.onChangeInput}
        />
        <Button name="Save" className="btn-save" onClick={this.save} />
      </form>
    );
  }
}
