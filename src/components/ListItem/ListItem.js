import React, { Component } from "react";

export default class ListItem extends Component {
  onClick = () => {
    if (this.props.onClick) {
      this.props.onClick(this.props.name);
    }
  };

  render() {
    const name = this.props.name;

    return (
      <li className="list-item" onClick={this.onClick}>
        {name}
      </li>
    );
  }
}
