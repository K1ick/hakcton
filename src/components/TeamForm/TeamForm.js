import React, { Component } from "react";
import Button from "components/Button";
import { addCommand } from "../../utils/fetch-api";

export default class TeamForm extends Component {
  state = {
    name: "",
    agent: { name: "", email: "" }
  };

  onChangeInput = e => {
    this.setState({ [e.target.name]: e.target.value });
  };

  onChangeAgentInput = e => {
    this.setState({
      agent: { ...this.state.agent, [e.target.name]: e.target.value }
    });
  };

  save = () => {
    addCommand(this.state);
  };

  render() {
    return (
      <form className="form">
        <label for="name">Team's name:</label>
        <input
          className="form-input"
          id="name"
          name="name"
          onChange={this.onChangeInput}
        />
        <label for="agentName">Agent's name:</label>
        <input
          className="form-input"
          id="agentName"
          name="name"
          onChange={this.onChangeAgentInput}
        />
        <label for="email">Agent's email:</label>
        <input
          className="form-input"
          id="email"
          name="email"
          onChange={this.onChangeAgentInput}
        />
        <Button className="btn-save" name="Save" onClick={this.save} />
      </form>
    );
  }
}
