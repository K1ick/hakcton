import React, { Component } from "react";
import Search from "components/Search";
import "./Header";

export default class Header extends Component {
  render() {
    return <Search results={this.props.results} getSearchResults={this.props.getSearchResults} resetSearchResultsState={this.props.resetSearchResultsState}/>;
  }
}
