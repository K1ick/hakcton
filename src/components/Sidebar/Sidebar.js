import React, { Component } from "react";
import RegistryList from "components/RegistryList";
import "./Sidebar.css";

class Sidebar extends Component {
  render() {
    return (
      <div className="sidebar">
        <h1>Tags</h1>
        <RegistryList
          items={this.props.items}
          onClick={this.props.getMicros}
          openedTags={this.props.openedTags}
        />
      </div>
    );
  }
}

export default Sidebar;
