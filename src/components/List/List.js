import React, { Component } from "react";
import LinkItem from "components/LinkItem";

export default class List extends Component {
  render() {
    const { items } = this.props;
    return (
      <ul>
        {items.map(item => {
          return (
            <LinkItem
              key={Math.random()
                .toString()
                .slice(2)}
              name={item}
            />
          );
        })}
      </ul>
    );
  }
}
