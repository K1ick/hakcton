import React, { Component } from "react";
import Button from "components/Button";
import List from "components/List";
import "./Search.css";

export default class Search extends Component {
  constructor(props) {
    super(props);
    this.searchElem = React.createRef();
  }
  state = {
    input: "",
    show: false,
  };

  onClick = () => {
    this.props.getSearchResults(this.state.input);
  }

  shouldComponentUpdate(nextProps, nextState) {
    return nextState.input!==this.state.input;
  }

  componentDidUpdate() {
    if(this.state.input.length>3) {
      this.props.getSearchResults(this.state.input);
    }
    if(this.props.results.length > 0) {
      this.setState({
        show: true
      })
      document.addEventListener('click', e=>{
        if(!e.composedPath().includes(this.searchElem.current)) {
          this.props.resetSearchResultsState()
        }
      })
    }
  }

  onChangeInput = e => {
    this.setState({ input: e.target.value });
  };

  render() {
    const suggestClasses = this.state.show ? "search-suggests" : "search-suggests hidden";
    return (
      <div ref={this.searchElem} className="search-wrapper">
        <div className="search">
          <input
            className="search-input"
            value={this.state.input}
            onChange={this.onChangeInput}
            placeholder="Search microservice"
          />
          <Button name="Search" onClick={this.onClick} />
        </div>
        <div className={suggestClasses}>
          <List items={this.props.results} />
        </div>
      </div>
    );
  }
}
