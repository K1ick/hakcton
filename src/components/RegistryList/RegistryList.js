import React, { Component, Fragment } from "react";
import ListItem from "components/ListItem";
import LinkItem from "components/LinkItem";
import "./RegistryList.css";

export default class RegistryList extends Component {
  render() {
    const { items } = this.props;
    const { openedTags } = this.props;

    return (
      <ul>
        {items
          ? items.map(name => {
              return (
                <Fragment
                  key={Math.random()
                    .toString()
                    .slice(2)}
                >
                  <ListItem
                    name={name}
                    onClick={this.props.onClick}
                    openedTags={openedTags}
                  />
                  <ul>
                    {openedTags.filter(openedTag => name === openedTag.name)[0]
                      ? openedTags
                          .filter(openedTag => name === openedTag.name)[0]
                          .micros.map(name => (
                            <LinkItem
                              key={Math.random()
                                .toString()
                                .slice(2)}
                              name={name}
                            />
                          ))
                      : null}
                  </ul>
                </Fragment>
              );
            })
          : null}
      </ul>
    );
  }
}
