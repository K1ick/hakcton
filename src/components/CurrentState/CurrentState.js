import React, { Component } from 'react';
import "./CurrentState.css"

export default class CurrentState extends Component {
    render() {
        let stateObject = {
            dev: {
                class: "state dev",
                text: "In develop"
            },
            depricated: {
                class: "state depricated",
                text: "Depricated"
            },
            normal: {
                class: "state",
                text: "Normal"
            }
        }
        let stateClass = '';
        let stateText = '';
        if(stateObject[this.props.current]) {
            stateClass = stateObject[this.props.current].class || 'state err';
            stateText = stateObject[this.props.current].text || 'Error';
        }
        return (
            <span className={stateClass}>{stateText}</span>
        )
    }
}
