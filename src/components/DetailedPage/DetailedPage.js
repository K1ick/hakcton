import React, { Component } from "react";
import { Link } from 'react-router-dom';
import { getDetails, getCommand } from "../../utils/fetch-api";
import CurrentState from "../CurrentState/CurrentState";
import MethodDescription from "../MethodDescription/MethodDescription";
import Tag from "../Tag/Tag";
import "./DetailedPage.css";

export default class DetailedPage extends Component {
  state = {
    name: "",
    description: "",
    respTeam: "",
    tags: [],
    methods: [],
    related: [],
    current: "",
    url: "",
    agent: {}
  };

  componentDidUpdate() {
    if (this.state.name !== this.props.match.params.microName) {
      getDetails(this.props.match.params.microName).then(({ details }) => {
        this.setState({ ...details });
      });
    }
  }

  componentDidMount() {
    getDetails(this.props.match.params.microName).then(({ details }) => {
        this.setState({ ...details });
      });
  }

  handleCommandNameClick = () => {
      getCommand(this.state.name).then(({agent}) => {
        this.setState({agent});
      });
  }

    render() {
        const teamClasses = this.state.agent.name ? "team-wrapper" : "team-wrapper hidden";
        return (
            <div className="details-wrapper">
                <div className="name-wrapper">
                    <h2>{this.state.name}</h2>
                    <div className="respteam" onClick={this.handleCommandNameClick}>({this.state.respTeam})</div>
                    <CurrentState current={this.state.current}></CurrentState>
                </div>
                <div className={teamClasses}>
                    <span>{this.state.agent.name}</span>
                    <span className="agent-email">{this.state.agent.email}</span>
                </div>
                <div className="tag-wrapper">{this.state.tags.map(tag=>{
                    return <Tag key={Math.random().toString().slice(2)}>{tag}</Tag>
                })}</div>
                <p className="micro-descr">{this.state.description}</p>
                <span className="url">Microservice URL: {this.state.url}</span>
                <div className="methods-wrapper">{this.state.methods.map(
                    method => {
                        return <MethodDescription key={Math.random().toString().slice(2)} method={method}/>
                    }
                )}</div>
                <h3 className="related-header">Связанные микросервисы</h3>
                <div className="related-wrapper">{this.state.related.map(
                    micro => {
                        return <div className="related">
                            <Link to={'/'+micro.name} className="related-link">{micro.name}</Link>
                            <p className="related-desc">{micro.description}</p>
                        </div>
                    } 
                )}</div>
            </div>
        )
    }
}
