import React, { Component } from "react";
import List from "components/List";

export default class SearchResults extends Component {
  componentWillUnmount() {
    this.props.resetSearchResultsState();
  }

  render() {
    return <List items={this.props.results} />;
  }
}
