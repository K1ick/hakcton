import React from "react";
import Sidebar from "components/Sidebar";
import DetailedPage from "../DetailedPage";
import Header from "components/Header";
import SearchResults from "components/SearchResults";
import TeamForm from "components/TeamForm";
import MicroserviceForm from "components/MicroserviceForm";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import { getTags, getMicros, searchQuery } from "utils/fetch-api";
import "./App.css";

export default class App extends React.Component {
  state = {
    tags: [],
    openedTags: [],
    searchResults: []
  };

  resetSearchResultsState = () => {
    this.setState({ searchResults: [] });
  };

  tagClickHandler = name => {
    getMicros(name).then(({ services }) => {
      this.setState({
        openedTags: [
          {
            name,
            micros: [...services]
          }
        ]
      });
    });
  };

  getSearchResults = input => {
    searchQuery(input).then(({ matches }) => {
      this.setState({
        searchResults: [...matches]
      });
    });
  };

  componentDidMount() {
    getTags().then(({ tags }) => {
      this.setState({ tags });
    });
  }

  render() {
    return (
      <div className="grid-wrapper">
        <Router>
          <Sidebar
            className="sidebar"
            items={this.state.tags}
            getMicros={this.tagClickHandler}
            openedTags={this.state.openedTags}
          />
          <Header className="header" results={this.state.searchResults} getSearchResults={this.getSearchResults} resetSearchResultsState={this.resetSearchResultsState} />
          <div className="content">
          <Switch>
                <Route
                  exact
                  path="/"
                  render={() => (
                    <div className="content-placeholder">
                      Выберите микросервис из списка слева, или начните вбивать
                      название в поиске.
                    </div>
                  )}
                />
                <Route exact path="/:microName" component={DetailedPage} />
                <Route path="/add/team" component={TeamForm} />
                <Route path="/add/microservice" component={MicroserviceForm} />
              </Switch>
          </div>
        </Router>
      </div>
    );
  }
}
