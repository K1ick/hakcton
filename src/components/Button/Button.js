import React, { Component } from "react";
import "./Button.css";

export default class Button extends Component {
  onClick = () => {
    if (this.props.onClick) {
      this.props.onClick();
    }
  };

  render() {
    const { name } = this.props;
    return (
      <button className="button" onClick={this.onClick}>
        {name}
      </button>
    );
  }
}
